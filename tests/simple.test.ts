import { beforeEach, describe, expect, it as test } from 'vitest';
import got from 'got';
import { compare_by_id } from './helpers';

const host = 'https://l761dniu80.execute-api.us-east-2.amazonaws.com';
const URI = host + '/default/exercise_api';

export interface Data {
  value: string;
  main_key: string;
}

// CRUD operations
const create = got.put;
const read = got.get;
const update = got.post;
const del = got.delete;

beforeEach(clear_db);

describe('@create', () => {
  test('create returns 201 status code on creation', async () => {
    // https://datatracker.ietf.org/doc/html/rfc7231#section-4.3.4
    const new_item: Data = { value: 'test', main_key: 'test' };

    const response = await create(URI, { json: new_item });
    expect(response.statusCode).toBe(201);
  });

  test('creates new item', async () => {
    const new_item: Data = { value: 'Boom', main_key: 'test' };

    const data = await create(URI, { json: new_item }).json<Data>();
    expect(data).toEqual(new_item);

    const all_data = await read(URI).json<[Data]>();
    expect(all_data).toHaveLength(1);
    expect(all_data[0]).toEqual(new_item);
  });

  test('create respects quota of 10 items', async () => {
    const state: Data[] = [];
    for (let i = 0; i < 10; i++) {
      state.push({ value: 'test', main_key: `test${i}` });
    }
    await set_state(state);

    const new_item: Data = { value: 'Boom', main_key: 'new_item' };

    const response = await create(URI, {
      json: new_item,
      throwHttpErrors: false,
    });
    expect(response.statusCode).toBe(400);
    expect(response.body).toBe('you reached your quta');

    const all_data = await read(URI).json<[Data]>();
    expect(all_data.sort(compare_by_id)).toEqual(state.sort(compare_by_id));
  }, 15_000);
});

describe('@read', () => {
  test('read empty store', async () => {
    const data = await read(URI).json<[Data]>();
    expect(data).toHaveLength(0);
  });

  test('reads all data in the store', async () => {
    const state: Data[] = [
      { value: '1', main_key: 'a' },
      { value: '2', main_key: 'b' },
      { value: '3', main_key: 'c' },
      { value: '4', main_key: 'd' },
    ];
    await set_state(state);

    const data = await read(URI).json<[Data]>();
    expect(data.sort(compare_by_id)).toEqual(state.sort(compare_by_id));
  });
});

describe('@update', () => {
  test('updates item', async () => {
    const existing_item: Data = { value: 'Boom', main_key: 'test' };
    await set_state([existing_item]);

    const updated_item: Data = { ...existing_item, value: 'Bada Boom' };

    const response = await update(URI, { json: updated_item });
    expect(response.statusCode).toBe(200);

    const data = JSON.parse(response.body) as Data;
    expect(data).toEqual(updated_item);

    const all_data = await read(URI).json<[Data]>();
    expect(all_data).toHaveLength(1);
    expect(all_data[0]).toEqual(updated_item);
  });

  test('does not update non-existing item', async () => {
    const updated_item: Data = { value: 'Bada Boom', main_key: 'test' };

    const response = await update(URI, {
      json: updated_item,
      throwHttpErrors: false,
    });
    // ???
    // 404 makes more sense than 400
    expect(response.statusCode).toBe(400);
  });

  test("update doesn't affect other items", async () => {
    const existing_item: Data = { value: 'Boom', main_key: 'test' };
    const other_items: Data[] = [{ value: 'Fire', main_key: 'test2' }];

    const state = [...other_items, existing_item];
    await set_state(state);

    const updated_item: Data = { ...existing_item, value: 'Bada Boom' };

    const data = await update(URI, { json: updated_item }).json<Data>();
    expect(data).toEqual(updated_item);

    const all_data = await read(URI).json<[Data]>();
    const updated_state = [...other_items, updated_item];

    expect(all_data.sort(compare_by_id)).toEqual(
      updated_state.sort(compare_by_id)
    );
  });
});

describe('@delete', () => {
  test('deletes item', async () => {
    const existing_item: Data = { value: 'Boom', main_key: 'test' };
    await set_state([existing_item]);

    const response = await del(URI, {
      json: { main_key: existing_item.main_key },
    });
    expect(response.statusCode).toBe(200);
    // ???
    // From docs: "Return value: the new value deleted"
    // means should be full key-value pair of the deleted item
    // expect(JSON.parse(response.body)).toEqual(existing_item);
    expect(JSON.parse(response.body)).toEqual({
      main_key: existing_item.main_key,
    });

    const data = await read(URI).json<[Data]>();
    expect(data).toHaveLength(0);
  });

  test('does not delete non-existing item', async () => {
    const existing_item: Data = { value: 'Boom', main_key: 'test' };
    await set_state([existing_item]);

    const response = await del(URI, { json: { main_key: 'test2' } });
    // ???
    // From docs: "Errors: 5XX server error"
    // 404 makes more sense -> should update the requirements
    expect(response.statusCode).toBe(404);

    const data = await read(URI).json<[Data]>();
    expect(data).toHaveLength(1);
  });

  test('does not affect other items', async () => {
    const existing_item: Data = { value: 'Boom', main_key: 'test' };
    const other_items: Data[] = [{ value: 'Fire', main_key: 'test2' }];

    const state = [...other_items, existing_item];
    await set_state(state);

    const response = await del(URI, {
      json: { main_key: existing_item.main_key },
    });
    expect(response.statusCode).toBe(200);

    const data = await read(URI).json<[Data]>();
    expect(data.sort(compare_by_id)).toEqual(other_items.sort(compare_by_id));
  });

  // TODO: malformed request? (without main_key)
});

async function clear_db() {
  const data = await read(URI).json<[Data]>();
  for (const { main_key } of data) {
    await del(URI, { json: { main_key } });
  }
}

async function set_state(state: Data[], db_is_empty = true) {
  if (!db_is_empty) {
    await clear_db();
  }

  for (const item of state) {
    await create(URI, { json: item });
  }
}
