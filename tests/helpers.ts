import { Data } from './simple.test';

export function compare_by_id(a: Data, b: Data) {
  if (a.main_key < b.main_key) {
    return -1;
  }

  if (a.main_key > b.main_key) {
    return 1;
  }

  return 0;
}
