# AutoTest excercise

CRUD operation of a REST API

## how to use

1. install bun (Linux or Mac OS; Windows supports installation via WSL but I read reports that it is buggy)

```sh
$ curl https://bun.sh/install | bash
```

2. install dependencies

```sh
$ bun install
```

3. run tests

```sh
$ bun test
```

## [bun](https://bun.sh/)

Sorry for using bun runtime.
I couldn't resist to try something new considering that it almost implements nodejs API and is "blazingly" fast. Plus it supports typescript out of the box.

## [vitest](https://vitest.dev/)

Great test runner with almost zero config and jest-like API. The only thing I had to configure is to disable concurrent run for tests in different describe blocks, because it has no sense to run in parallel for a resource that cannot be splitted.

## [got](https://github.com/sindresorhus/got)

This is a nice HTTP request library. It has nice API and it is written in typescript.

---

## Missunderstandings

I had issues with the task. I could not understand if we are going to support [HTTP](https://datatracker.ietf.org/doc/html/rfc7231) specification for the endpoint. Because the usage of POST is more like PUT without creation of the resource if it is absent. And PUT is a POST. We have to rely on the specification because it is not feasible to describe endpoints fully in the requirements doc. But the requirements are clearly contradict HTTP specification.

---

## Solution

That why I decided to make these bindings, because they match the best to the requirements:

- Create = PUT
- Read = GET
- Update = POST
- Delete = DELETE

After that I added a few tests for each of CRUD operations. I don't think the list of tests is complete but it gives the basis.

---

## Possible improvements

- More test cases: e.g. how SUT behaves with incorrect data.
- The testfile became too big, thus it would be also nice to split the file into multiple files for each operation.
- The tests are ruther slow. That is because I didn't want the tests to be dependend on each other. I had to make requests to delete items from the store and insert new data specific to the test. Because we don't have bulk operations (Create and Delete) I had to make many requests. In the test for checking `quota` I even had to increase default timeout to 15s. This is probably not going to be the case in the real world application where we would have greater options with resource manipulations.
- we can also create gitlab pipeline for running tests in CI (check [my other tech task repo](https://gitlab.com/tamagisan/tech-task-affindi/-/blob/main/.gitlab-ci.yml) for the example)
